# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the ArcMenu package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ArcMenu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-13 18:34-0500\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: appMenu.js:69 appMenu.js:332
msgid "Pin to ArcMenu"
msgstr ""

#: appMenu.js:98 appMenu.js:287
msgid "Create Desktop Shortcut"
msgstr ""

#: appMenu.js:286
msgid "Delete Desktop Shortcut"
msgstr ""

#: appMenu.js:295 appMenu.js:332
msgid "Unpin from ArcMenu"
msgstr ""

#: appMenu.js:371
msgid "Open Folder Location"
msgstr ""

#: constants.js:79
msgid "Favorites"
msgstr ""

#: constants.js:80 menulayouts/runner.js:130 menulayouts/windows.js:322
#: settings/Menu/LayoutTweaksPage.js:784
msgid "Frequent Apps"
msgstr ""

#: constants.js:81 menuWidgets.js:1115 menulayouts/az.js:61
#: menulayouts/az.js:70 menulayouts/eleven.js:53 menulayouts/eleven.js:62
#: menulayouts/redmond.js:72 menulayouts/redmond.js:75
#: menulayouts/redmond.js:79
msgid "All Apps"
msgstr ""

#: constants.js:82 menulayouts/insider.js:271
#: settings/Menu/LayoutTweaksPage.js:672 settings/Menu/LayoutTweaksPage.js:782
#: settings/MenuPage.js:101
msgid "Pinned Apps"
msgstr ""

#: constants.js:83 menulayouts/raven.js:293 menulayouts/unity.js:365
#: searchProviders/recentFiles.js:29 searchProviders/recentFiles.js:30
msgid "Recent Files"
msgstr ""

#: constants.js:237 menuButton.js:732 settings/Menu/ListPinnedPage.js:317
msgid "Log Out..."
msgstr ""

#: constants.js:238 menuButton.js:731 settings/Menu/ListPinnedPage.js:316
msgid "Lock"
msgstr ""

#: constants.js:239 menuButton.js:726 settings/Menu/ListPinnedPage.js:319
msgid "Restart..."
msgstr ""

#: constants.js:240 menuButton.js:727 settings/Menu/ListPinnedPage.js:318
msgid "Power Off..."
msgstr ""

#: constants.js:241 menuButton.js:723 settings/Menu/ListPinnedPage.js:320
msgid "Suspend"
msgstr ""

#: constants.js:242 settings/Menu/ListPinnedPage.js:321
msgid "Hybrid Sleep"
msgstr ""

#: constants.js:243 settings/Menu/ListPinnedPage.js:323
msgid "Hibernate"
msgstr ""

#: constants.js:244 menuButton.js:733 settings/Menu/ListPinnedPage.js:324
msgid "Switch User"
msgstr ""

#: constants.js:387 settings/AboutPage.js:41 settings/GeneralPage.js:519
msgid "ArcMenu"
msgstr ""

#: constants.js:392
msgid "Brisk"
msgstr ""

#: constants.js:397
msgid "Whisker"
msgstr ""

#: constants.js:402
msgid "GNOME Menu"
msgstr ""

#: constants.js:407
msgid "Mint"
msgstr ""

#: constants.js:412
msgid "Budgie"
msgstr ""

#: constants.js:420
msgid "Unity"
msgstr ""

#: constants.js:425
msgid "Plasma"
msgstr ""

#: constants.js:430
msgid "tognee"
msgstr ""

#: constants.js:435
msgid "Insider"
msgstr ""

#: constants.js:440
msgid "Redmond"
msgstr ""

#: constants.js:445
msgid "Windows"
msgstr ""

#: constants.js:450
msgid "11"
msgstr ""

#: constants.js:455
msgid "a.z."
msgstr ""

#: constants.js:460
msgid "Enterprise"
msgstr ""

#: constants.js:465
msgid "Pop"
msgstr ""

#: constants.js:473
msgid "Elementary"
msgstr ""

#: constants.js:478
msgid "Chromebook"
msgstr ""

#: constants.js:486
msgid "Runner"
msgstr ""

#: constants.js:491
msgid "GNOME Overview"
msgstr ""

#: constants.js:499
msgid "Raven"
msgstr ""

#: constants.js:507
msgid "Traditional"
msgstr ""

#: constants.js:512
msgid "Modern"
msgstr ""

#: constants.js:517
msgid "Touch"
msgstr ""

#: constants.js:522
msgid "Launcher"
msgstr ""

#: constants.js:527
msgid "Alternative"
msgstr ""

#: constants.js:534
msgid "Settings"
msgstr ""

#: constants.js:534
msgid "Software"
msgstr ""

#: constants.js:534
msgid "Terminal"
msgstr ""

#: constants.js:534
msgid "Tweaks"
msgstr ""

#: constants.js:535 menuButton.js:676 menuWidgets.js:489
#: settings/Menu/ListPinnedPage.js:286 settings/Menu/ListPinnedPage.js:306
msgid "Activities Overview"
msgstr ""

#: constants.js:535 menuButton.js:662 prefs.js:24 settings/AboutPage.js:134
#: settings/Menu/ListPinnedPage.js:274 settings/Menu/ListPinnedPage.js:283
#: settings/Menu/ListPinnedPage.js:293
msgid "ArcMenu Settings"
msgstr ""

#: constants.js:535
msgid "Files"
msgstr ""

#: menuButton.js:39 menulayouts/plasma.js:122 menulayouts/plasma.js:555
msgid "Apps"
msgstr ""

#: menuButton.js:707 settings/Menu/ListPinnedPage.js:309
msgid "Show Desktop"
msgstr ""

#: menuButton.js:721 menuWidgets.js:802 menuWidgets.js:819
msgid "Power Off / Log Out"
msgstr ""

#: menuButton.js:762
msgid "Dash to Panel Settings"
msgstr ""

#: menuButton.js:766
msgid "App Icons Taskbar Settings"
msgstr ""

#: menuWidgets.js:856 menulayouts/plasma.js:319
msgid "Session"
msgstr ""

#: menuWidgets.js:860 menulayouts/plasma.js:323
msgid "System"
msgstr ""

#: menuWidgets.js:1045 menuWidgets.js:1066 menulayouts/az.js:62
#: menulayouts/eleven.js:54 menulayouts/redmond.js:73 menulayouts/redmond.js:78
msgid "Back"
msgstr ""

#: menuWidgets.js:2297 menulayouts/pop.js:339
msgid "Unnamed Folder"
msgstr ""

#: menuWidgets.js:2392 menulayouts/pop.js:1151
msgid "New"
msgstr ""

#: menuWidgets.js:2924 menulayouts/az.js:69 menulayouts/eleven.js:61
#: menulayouts/insider.js:176 menulayouts/plasma.js:117 menulayouts/raven.js:62
#: menulayouts/raven.js:230 menulayouts/raven.js:271 menulayouts/redmond.js:74
#: menulayouts/redmond.js:77 menulayouts/redmond.js:80 menulayouts/unity.js:40
#: menulayouts/unity.js:273 menulayouts/unity.js:343 menulayouts/windows.js:398
msgid "Pinned"
msgstr ""

#: menuWidgets.js:3089
msgid "Unmount Drive"
msgstr ""

#: menuWidgets.js:3091
msgid "Eject Drive"
msgstr ""

#: menuWidgets.js:3135
msgid "Remove from Recent"
msgstr ""

#: menuWidgets.js:3217
msgid "Search…"
msgstr ""

#: menulayouts/baseMenuLayout.js:481
msgid "More Recent Files..."
msgstr ""

#: menulayouts/eleven.js:69
msgid "Frequent"
msgstr ""

#: menulayouts/plasma.js:126 placeDisplay.js:131 placeDisplay.js:154
#: settings/Menu/ListPinnedPage.js:340
msgid "Computer"
msgstr ""

#: menulayouts/plasma.js:130
msgid "Leave"
msgstr ""

#: menulayouts/plasma.js:267 menulayouts/windows.js:259
#: settings/MenuPage.js:123
msgid "Application Shortcuts"
msgstr ""

#: menulayouts/plasma.js:272 menulayouts/windows.js:263
msgid "Places"
msgstr ""

#: menulayouts/plasma.js:287 menulayouts/windows.js:195
#: settings/Menu/LayoutTweaksPage.js:719 settings/Menu/LayoutTweaksPage.js:830
msgid "Bookmarks"
msgstr ""

#: menulayouts/plasma.js:289 menulayouts/windows.js:197
msgid "Devices"
msgstr ""

#: menulayouts/plasma.js:291 menulayouts/windows.js:199
#: settings/Menu/ListPinnedPage.js:341
msgid "Network"
msgstr ""

#: menulayouts/pop.js:244 menulayouts/pop.js:443 menulayouts/pop.js:456
msgid "Library Home"
msgstr ""

#: menulayouts/pop.js:694
msgid "New Folder"
msgstr ""

#: menulayouts/pop.js:789
msgid "Rename Folder"
msgstr ""

#: menulayouts/pop.js:790
msgid "Delete Folder"
msgstr ""

#: menulayouts/pop.js:800
#, javascript-format
msgid "Permanently delete %s folder?"
msgstr ""

#: menulayouts/pop.js:805
msgid "No"
msgstr ""

#: menulayouts/pop.js:813
msgid "Yes"
msgstr ""

#: menulayouts/pop.js:828
#, javascript-format
msgid "Rename %s folder"
msgstr ""

#: menulayouts/pop.js:862 settings/GeneralPage.js:317
msgid "Cancel"
msgstr ""

#: menulayouts/pop.js:870 settings/GeneralPage.js:305
#: settings/Menu/ListPinnedPage.js:574 settings/Menu/VisualSettings.js:365
msgid "Apply"
msgstr ""

#: menulayouts/raven.js:62 menulayouts/raven.js:236 menulayouts/unity.js:40
#: menulayouts/unity.js:277 settings/Menu/LayoutTweaksPage.js:573
#: settings/Menu/LayoutTweaksPage.js:600 settings/Menu/LayoutTweaksPage.js:671
#: settings/Menu/LayoutTweaksPage.js:760 settings/Menu/LayoutTweaksPage.js:785
msgid "All Programs"
msgstr ""

#: menulayouts/raven.js:277 menulayouts/unity.js:348
#: settings/Menu/VisualSettings.js:224
msgid "Shortcuts"
msgstr ""

#: menulayouts/runner.js:217
msgid "Configure Runner"
msgstr ""

#: menulayouts/unity.js:412
msgid "Categories"
msgstr ""

#: menulayouts/windows.js:420
msgid "Extras"
msgstr ""

#: placeDisplay.js:54
#, javascript-format
msgid "Failed to launch “%s”"
msgstr ""

#: placeDisplay.js:69
#, javascript-format
msgid "Failed to mount volume for “%s”"
msgstr ""

#: placeDisplay.js:117 placeDisplay.js:340
#: settings/Menu/LayoutTweaksPage.js:572 settings/Menu/LayoutTweaksPage.js:599
#: settings/Menu/ListPinnedPage.js:332 utils.js:285
msgid "Home"
msgstr ""

#: placeDisplay.js:221
#, javascript-format
msgid "Ejecting drive “%s” failed:"
msgstr ""

#: prefsWidgets.js:266
msgid "Modify"
msgstr ""

#: prefsWidgets.js:281
msgid "Move Up"
msgstr ""

#: prefsWidgets.js:288
msgid "Move Down"
msgstr ""

#: prefsWidgets.js:295
msgid "Remove"
msgstr ""

#: search.js:724
msgid "Searching..."
msgstr ""

#: search.js:726
msgid "No results."
msgstr ""

#: search.js:808
#, javascript-format
msgid "+ %d more"
msgstr ""

#: searchProviders/openWindows.js:31
msgid "List of open windows across all workspaces"
msgstr ""

#: searchProviders/openWindows.js:32
msgid "Open Windows"
msgstr ""

#: searchProviders/openWindows.js:46
#, javascript-format
msgid "'%s' on Workspace %d"
msgstr ""

#: searchProviders/recentFiles.js:90
#, javascript-format
msgid "Failed to open “%s”"
msgstr ""

#: settings/AboutPage.js:16 settings/Menu/ListPinnedPage.js:303
msgid "About"
msgstr ""

#: settings/AboutPage.js:22
msgid "Application Menu Extension for GNOME"
msgstr ""

#: settings/AboutPage.js:65
msgid "ArcMenu Version"
msgstr ""

#: settings/AboutPage.js:75
msgid "Git Commit"
msgstr ""

#: settings/AboutPage.js:85
msgid "GNOME Version"
msgstr ""

#: settings/AboutPage.js:94
msgid "OS Name"
msgstr ""

#: settings/AboutPage.js:107
msgid "Windowing System"
msgstr ""

#: settings/AboutPage.js:120
msgid "Website"
msgstr ""

#: settings/AboutPage.js:123
msgid "Report an Issue"
msgstr ""

#: settings/AboutPage.js:126
msgid "Donate via PayPal"
msgstr ""

#: settings/AboutPage.js:137 settings/Menu/ThemingDialog.js:71
#: settings/Menu/ThemingDialog.js:76
msgid "Load"
msgstr ""

#: settings/AboutPage.js:142
msgid "Load Settings"
msgstr ""

#: settings/AboutPage.js:168 settings/Menu/ThemingDialog.js:129
#: settings/Menu/ThemingDialog.js:142
msgid "Save"
msgstr ""

#: settings/AboutPage.js:173
msgid "Save Settings"
msgstr ""

#: settings/AboutPage.js:196 settings/AboutPage.js:197
msgid "Credits"
msgstr ""

#: settings/AboutPage.js:201
msgid "Brought to you by"
msgstr ""

#: settings/AboutPage.js:212
msgid "History"
msgstr ""

#: settings/AboutPage.js:226
msgid "Contributions by"
msgstr ""

#: settings/AboutPage.js:229
msgid "Contributors"
msgstr ""

#: settings/AboutPage.js:231
msgid "Translators"
msgstr ""

#: settings/AboutPage.js:235
msgid "Artwork by"
msgstr ""

#: settings/AboutPage.js:243 settings/AboutPage.js:244
msgid "Legal"
msgstr ""

#: settings/AboutPage.js:250
msgid "This program comes with absolutely no warranty."
msgstr ""

#: settings/AboutPage.js:252
msgid "GNU General Public License, version 2 or later"
msgstr ""

#. TRANSLATORS: this is the program license url; the string contains the name of the license as link text.
#: settings/AboutPage.js:252
#, javascript-format
msgid "See the <a href=\"%s\">%s</a> for details."
msgstr ""

#: settings/GeneralPage.js:32
msgid "General"
msgstr ""

#: settings/GeneralPage.js:39
msgid "Panel Display Options"
msgstr ""

#: settings/GeneralPage.js:52
msgid "Show Activities Button"
msgstr ""

#: settings/GeneralPage.js:53
msgid "Dash to Panel may conflict with this setting"
msgstr ""

#: settings/GeneralPage.js:61 settings/GeneralPage.js:87
#: settings/Menu/LayoutTweaksPage.js:627
msgid "Left"
msgstr ""

#: settings/GeneralPage.js:62 settings/GeneralPage.js:88
msgid "Center"
msgstr ""

#: settings/GeneralPage.js:63 settings/GeneralPage.js:89
#: settings/Menu/LayoutTweaksPage.js:628
msgid "Right"
msgstr ""

#: settings/GeneralPage.js:65 settings/MenuButtonPage.js:124
msgid "Position in Panel"
msgstr ""

#: settings/GeneralPage.js:95
msgid "Menu Alignment"
msgstr ""

#: settings/GeneralPage.js:114
msgid "Display ArcMenu on all Panels"
msgstr ""

#: settings/GeneralPage.js:115
msgid "Dash to Panel or App Icons Taskbar extension required"
msgstr ""

#: settings/GeneralPage.js:131
msgid "Always Prefer Top Panel"
msgstr ""

#: settings/GeneralPage.js:132
msgid "Useful with Dash to Panel setting 'Keep original gnome-shell top panel'"
msgstr ""

#: settings/GeneralPage.js:146
msgid "General Settings"
msgstr ""

#: settings/GeneralPage.js:150 settings/GeneralPage.js:337
msgid "ArcMenu Hotkey"
msgstr ""

#: settings/GeneralPage.js:151
msgid "Standalone Runner Menu"
msgstr ""

#: settings/GeneralPage.js:164
msgid "Hide Overview on Startup"
msgstr ""

#: settings/GeneralPage.js:213
msgid "Hotkey Options"
msgstr ""

#: settings/GeneralPage.js:226 settings/GeneralPage.js:362
msgid "Disabled"
msgstr ""

#: settings/GeneralPage.js:251
msgid "Open on Primary Monitor"
msgstr ""

#: settings/GeneralPage.js:269
msgid "Modify Hotkey"
msgstr ""

#. TRANSLATORS: %s is replaced with a description of the keyboard shortcut, don't translate/transliterate <b>%s</b>
#: settings/GeneralPage.js:337
#, javascript-format
msgid "Enter a new shortcut to change <b>%s</b>"
msgstr ""

#: settings/GeneralPage.js:337
msgid "Standalone Runner Hotkey"
msgstr ""

#: settings/GeneralPage.js:367
msgid "Press Esc to cancel or Backspace to disable the keyboard shortcut."
msgstr ""

#: settings/GeneralPage.js:418
#, javascript-format
msgid "Conflict with <b>%s</b> hotkey"
msgstr ""

#: settings/GeneralPage.js:520
msgid "Standlone Runner"
msgstr ""

#: settings/Menu/FineTunePage.js:32
msgid "Show Category Sub Menus"
msgstr ""

#: settings/Menu/FineTunePage.js:46
msgid "Show Application Descriptions"
msgstr ""

#: settings/Menu/FineTunePage.js:60
msgid "Disable ScrollView Fade Effects"
msgstr ""

#: settings/Menu/FineTunePage.js:74
msgid "Disable Tooltips"
msgstr ""

#: settings/Menu/FineTunePage.js:88
msgid "Alphabetize 'All Programs' Category"
msgstr ""

#: settings/Menu/FineTunePage.js:102
msgid "Show Hidden Recent Files"
msgstr ""

#: settings/Menu/FineTunePage.js:116
msgid "Multi-Lined Labels"
msgstr ""

#: settings/Menu/FineTunePage.js:117
msgid "Allow application labels to span multiple lines on grid style layouts"
msgstr ""

#: settings/Menu/FineTunePage.js:127
msgid "Full Color"
msgstr ""

#: settings/Menu/FineTunePage.js:128
msgid "Symbolic"
msgstr ""

#: settings/Menu/FineTunePage.js:130
msgid "Category Icon Type"
msgstr ""

#: settings/Menu/FineTunePage.js:131 settings/Menu/FineTunePage.js:142
msgid "Some icon themes may not include selected icon type"
msgstr ""

#: settings/Menu/FineTunePage.js:141
msgid "Shortcuts Icon Type"
msgstr ""

#: settings/Menu/FineTunePage.js:165
msgid "Disable New Apps Tracker"
msgstr ""

#: settings/Menu/FineTunePage.js:174
msgid "Clear All"
msgstr ""

#: settings/Menu/FineTunePage.js:183
msgid "Clear Apps Marked 'New'"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:25 settings/MenuPage.js:49
#: settings/MenuPage.js:91
#, javascript-format
msgid "%s Layout Tweaks"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:110 settings/Menu/LayoutTweaksPage.js:115
#: settings/Menu/LayoutTweaksPage.js:693 settings/Menu/LayoutTweaksPage.js:805
msgid "Extra Shortcuts"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:135
msgid "Vertical Separator"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:144
msgid "Mouse Click"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:145
msgid "Mouse Hover"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:148
msgid "Category Activation"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:171
msgid "Round"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:172 settings/Menu/VisualSettings.js:178
#: settings/Menu/VisualSettings.js:179 settings/Menu/VisualSettings.js:180
msgid "Square"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:174
msgid "Avatar Icon Shape"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:190 settings/Menu/LayoutTweaksPage.js:851
msgid "Bottom"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:191 settings/Menu/LayoutTweaksPage.js:453
#: settings/Menu/LayoutTweaksPage.js:852
msgid "Top"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:194
msgid "Searchbar Location"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:215
msgid "Flip Layout Horizontally"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:231
msgid "Disable User Avatar"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:271 settings/Menu/LayoutTweaksPage.js:575
#: settings/Menu/LayoutTweaksPage.js:602 settings/Menu/LayoutTweaksPage.js:675
#: settings/Menu/LayoutTweaksPage.js:762 settings/Menu/LayoutTweaksPage.js:787
msgid "Default View"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:298 settings/Menu/LayoutTweaksPage.js:351
msgid "Disable Frequent Apps"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:332
msgid "Show Apps Grid"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:365
msgid "Disable Pinned Apps"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:391
msgid "Activate on Hover"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:441
msgid "Enable Activities Overview Shortcut"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:454
msgid "Centered"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:456
msgid "Position"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:467 settings/Menu/LayoutTweaksPage.js:613
msgid "List"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:468 settings/Menu/LayoutTweaksPage.js:614
msgid "Grid"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:470 settings/Menu/LayoutTweaksPage.js:616
msgid "Search Results Display Style"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:497 settings/Menu/VisualSettings.js:308
msgid "Width"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:520 settings/Menu/VisualSettings.js:36
#: settings/Menu/VisualSettings.js:327
msgid "Height"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:543 settings/Menu/ThemePage.js:168
msgid "Font Size"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:544 settings/MenuButtonPage.js:101
#, javascript-format
msgid "%d Default Theme Value"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:558
msgid "Show Frequent Apps"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:630
msgid "Position on Monitor"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:705 settings/Menu/LayoutTweaksPage.js:816
msgid "External Devices"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:749
msgid "Nothing Yet!"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:759 settings/Menu/LayoutTweaksPage.js:783
msgid "Categories List"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:838
msgid "Category Quick Links"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:839
msgid ""
"Display quick links of extra categories on the home page\n"
"Must also be enabled in 'Menu -> Extra Categories' section"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:854
msgid "Quick Links Location"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:886
msgid "Enable Weather Widget"
msgstr ""

#: settings/Menu/LayoutTweaksPage.js:900
msgid "Enable Clock Widget"
msgstr ""

#: settings/Menu/LayoutsPage.js:24
msgid "Current Menu Layout"
msgstr ""

#: settings/Menu/LayoutsPage.js:38
msgid "Choose a new menu layout?"
msgstr ""

#: settings/Menu/LayoutsPage.js:44
#, javascript-format
msgid "%s Menu Layouts"
msgstr ""

#: settings/Menu/ListOtherPage.js:34
msgid "Actions will be hidden from ArcMenu if not available on your system."
msgstr ""

#: settings/Menu/ListOtherPage.js:38
msgid "Power Off / Log Out Buttons"
msgstr ""

#: settings/Menu/ListOtherPage.js:41 settings/Menu/VisualSettings.js:111
#: settings/Menu/VisualSettings.js:177 settings/Menu/VisualSettings.js:271
msgid "Off"
msgstr ""

#: settings/Menu/ListOtherPage.js:42
msgid "Power Buttons"
msgstr ""

#: settings/Menu/ListOtherPage.js:43
msgid "Power Menu"
msgstr ""

#: settings/Menu/ListOtherPage.js:45
msgid "Override Display Style"
msgstr ""

#: settings/Menu/ListPinnedPage.js:29 settings/Menu/ListPinnedPage.js:32
#: settings/Menu/ListPinnedPage.js:38
msgid "Add More Apps"
msgstr ""

#: settings/Menu/ListPinnedPage.js:35
msgid "Add Default User Directories"
msgstr ""

#: settings/Menu/ListPinnedPage.js:41
msgid "Add More Shortcuts"
msgstr ""

#: settings/Menu/ListPinnedPage.js:100
msgid "Add Custom Shortcut"
msgstr ""

#: settings/Menu/ListPinnedPage.js:187
msgid "Invalid Shortcut"
msgstr ""

#: settings/Menu/ListPinnedPage.js:259
msgid "Add to your Pinned Apps"
msgstr ""

#: settings/Menu/ListPinnedPage.js:261
msgid "Add to your Extra Shortcuts"
msgstr ""

#: settings/Menu/ListPinnedPage.js:263
msgid "Select Application Shortcuts"
msgstr ""

#: settings/Menu/ListPinnedPage.js:265
msgid "Select Directory Shortcuts"
msgstr ""

#: settings/Menu/ListPinnedPage.js:267
msgid "Add to the Context Menu"
msgstr ""

#: settings/Menu/ListPinnedPage.js:285
msgid "Run Command..."
msgstr ""

#: settings/Menu/ListPinnedPage.js:288
msgid "Show All Apps"
msgstr ""

#: settings/Menu/ListPinnedPage.js:295
msgid "Menu Settings"
msgstr ""

#: settings/Menu/ListPinnedPage.js:297
msgid "Menu Theming"
msgstr ""

#: settings/Menu/ListPinnedPage.js:299
msgid "Change Menu Layout"
msgstr ""

#: settings/Menu/ListPinnedPage.js:301
msgid "Menu Button Settings"
msgstr ""

#: settings/Menu/ListPinnedPage.js:304
msgid "Panel Extension Settings"
msgstr ""

#: settings/Menu/ListPinnedPage.js:308 settings/MenuPage.js:144
msgid "Power Options"
msgstr ""

#: settings/Menu/ListPinnedPage.js:310 settings/Menu/ListPinnedPage.js:315
msgid "Separator"
msgstr ""

#: settings/Menu/ListPinnedPage.js:333
msgid "Documents"
msgstr ""

#: settings/Menu/ListPinnedPage.js:335
msgid "Downloads"
msgstr ""

#: settings/Menu/ListPinnedPage.js:337
msgid "Music"
msgstr ""

#: settings/Menu/ListPinnedPage.js:338
msgid "Pictures"
msgstr ""

#: settings/Menu/ListPinnedPage.js:339
msgid "Videos"
msgstr ""

#: settings/Menu/ListPinnedPage.js:342
msgid "Recent"
msgstr ""

#: settings/Menu/ListPinnedPage.js:369
msgid "Dash to Panel or App Icons Taskbar"
msgstr ""

#: settings/Menu/ListPinnedPage.js:451
#, javascript-format
msgid "%s has been added"
msgstr ""

#: settings/Menu/ListPinnedPage.js:462
#, javascript-format
msgid "%s has been removed"
msgstr ""

#: settings/Menu/ListPinnedPage.js:485
msgid "Add a Custom Shortcut"
msgstr ""

#: settings/Menu/ListPinnedPage.js:490
msgid "Edit Pinned App"
msgstr ""

#: settings/Menu/ListPinnedPage.js:492
msgid "Edit Shortcut"
msgstr ""

#: settings/Menu/ListPinnedPage.js:503
msgid "Title"
msgstr ""

#: settings/Menu/ListPinnedPage.js:515 settings/MenuButtonPage.js:57
msgid "Icon"
msgstr ""

#: settings/Menu/ListPinnedPage.js:527 settings/MenuButtonPage.js:156
#: settings/MenuButtonPage.js:473
msgid "Browse..."
msgstr ""

#: settings/Menu/ListPinnedPage.js:533 settings/MenuButtonPage.js:478
msgid "Select an Icon"
msgstr ""

#: settings/Menu/ListPinnedPage.js:562
msgid "Command"
msgstr ""

#: settings/Menu/ListPinnedPage.js:562
msgid "Directory"
msgstr ""

#: settings/Menu/ListPinnedPage.js:574
msgid "Add"
msgstr ""

#: settings/Menu/SearchOptionsPage.js:22
msgid "Extra Search Providers"
msgstr ""

#: settings/Menu/SearchOptionsPage.js:33
msgid "Search for open windows across all workspaces"
msgstr ""

#: settings/Menu/SearchOptionsPage.js:47
msgid "Search for recent files"
msgstr ""

#: settings/Menu/SearchOptionsPage.js:55 settings/MenuPage.js:134
msgid "Search Options"
msgstr ""

#: settings/Menu/SearchOptionsPage.js:66
msgid "Show Search Result Descriptions"
msgstr ""

#: settings/Menu/SearchOptionsPage.js:81
msgid "Highlight search result terms"
msgstr ""

#: settings/Menu/SearchOptionsPage.js:100
msgid "Max Search Results"
msgstr ""

#: settings/Menu/SearchOptionsPage.js:144
msgid "Search Box Border Radius"
msgstr ""

#: settings/Menu/SubPage.js:44 settings/MenuButtonPage.js:27
msgid "Reset settings"
msgstr ""

#: settings/Menu/SubPage.js:49 settings/MenuButtonPage.js:35
#, javascript-format
msgid "Reset all %s settings?"
msgstr ""

#: settings/Menu/SubPage.js:50 settings/MenuButtonPage.js:36
#, javascript-format
msgid "All %s settings will be reset to the default value."
msgstr ""

#: settings/Menu/ThemePage.js:38
msgid "Override Theme"
msgstr ""

#: settings/Menu/ThemePage.js:39 settings/MenuButtonPage.js:202
msgid "Results may vary with third party themes"
msgstr ""

#: settings/Menu/ThemePage.js:46 settings/Menu/ThemingDialog.js:68
#: settings/Menu/ThemingDialog.js:76 settings/Menu/ThemingDialog.js:142
msgid "Menu Themes"
msgstr ""

#: settings/Menu/ThemePage.js:90
msgid "Current Theme"
msgstr ""

#: settings/Menu/ThemePage.js:118
msgid "Save as Theme"
msgstr ""

#: settings/Menu/ThemePage.js:148
msgid "Menu Styling"
msgstr ""

#: settings/Menu/ThemePage.js:153 settings/Menu/ThemePage.js:179
#: settings/Menu/ThemePage.js:185 settings/MenuButtonPage.js:209
#: settings/MenuButtonPage.js:212 settings/MenuButtonPage.js:218
msgid "Background Color"
msgstr ""

#: settings/Menu/ThemePage.js:156 settings/Menu/ThemePage.js:182
#: settings/Menu/ThemePage.js:188 settings/MenuButtonPage.js:206
#: settings/MenuButtonPage.js:215 settings/MenuButtonPage.js:221
msgid "Foreground Color"
msgstr ""

#: settings/Menu/ThemePage.js:159 settings/MenuButtonPage.js:232
msgid "Border Color"
msgstr ""

#: settings/Menu/ThemePage.js:162 settings/MenuButtonPage.js:228
msgid "Border Width"
msgstr ""

#: settings/Menu/ThemePage.js:165 settings/MenuButtonPage.js:224
msgid "Border Radius"
msgstr ""

#: settings/Menu/ThemePage.js:171
msgid "Separator Color"
msgstr ""

#: settings/Menu/ThemePage.js:175
msgid "Menu Items Styling"
msgstr ""

#: settings/Menu/ThemePage.js:179 settings/Menu/ThemePage.js:182
#: settings/MenuButtonPage.js:212 settings/MenuButtonPage.js:215
msgid "Hover"
msgstr ""

#: settings/Menu/ThemePage.js:185 settings/Menu/ThemePage.js:188
#: settings/MenuButtonPage.js:218 settings/MenuButtonPage.js:221
msgid "Active"
msgstr ""

#: settings/Menu/ThemingDialog.js:16
msgid "Save Theme As..."
msgstr ""

#: settings/Menu/ThemingDialog.js:28
msgid "Theme Name"
msgstr ""

#: settings/Menu/ThemingDialog.js:45
msgid "Save Theme"
msgstr ""

#: settings/Menu/ThemingDialog.js:61
msgid "Manage Themes"
msgstr ""

#: settings/Menu/ThemingDialog.js:323
msgid "Save Themes"
msgstr ""

#: settings/Menu/ThemingDialog.js:325
msgid "Load Themes"
msgstr ""

#: settings/Menu/ThemingDialog.js:379
#, javascript-format
msgid "%s has been selected"
msgstr ""

#: settings/Menu/ThemingDialog.js:391
#, javascript-format
msgid "%s has been unselected"
msgstr ""

#: settings/Menu/VisualSettings.js:18
msgid "Menu Size"
msgstr ""

#: settings/Menu/VisualSettings.js:56
msgid "Left-Panel Width"
msgstr ""

#: settings/Menu/VisualSettings.js:57 settings/Menu/VisualSettings.js:78
msgid "Traditional Layouts"
msgstr ""

#: settings/Menu/VisualSettings.js:77
msgid "Right-Panel Width"
msgstr ""

#: settings/Menu/VisualSettings.js:98
msgid "Width Offset"
msgstr ""

#: settings/Menu/VisualSettings.js:99 settings/Menu/VisualSettings.js:186
msgid "Non-Traditional Layouts"
msgstr ""

#: settings/Menu/VisualSettings.js:106
msgid "Menu Location"
msgstr ""

#: settings/Menu/VisualSettings.js:112
msgid "Top Centered"
msgstr ""

#: settings/Menu/VisualSettings.js:113
msgid "Bottom Centered"
msgstr ""

#: settings/Menu/VisualSettings.js:115
msgid "Override Menu Location"
msgstr ""

#: settings/Menu/VisualSettings.js:156
msgid "Override Menu Rise"
msgstr ""

#: settings/Menu/VisualSettings.js:157
msgid "Menu Distance from Panel and Screen Edge"
msgstr ""

#: settings/Menu/VisualSettings.js:171
msgid "Override Icon Sizes"
msgstr ""

#: settings/Menu/VisualSettings.js:172
msgid "Override the icon size of various menu items"
msgstr ""

#: settings/Menu/VisualSettings.js:178 settings/Menu/VisualSettings.js:181
#: settings/Menu/VisualSettings.js:273
msgid "Small"
msgstr ""

#: settings/Menu/VisualSettings.js:179 settings/Menu/VisualSettings.js:182
#: settings/Menu/VisualSettings.js:274
msgid "Medium"
msgstr ""

#: settings/Menu/VisualSettings.js:180 settings/Menu/VisualSettings.js:183
#: settings/Menu/VisualSettings.js:275
msgid "Large"
msgstr ""

#: settings/Menu/VisualSettings.js:181 settings/Menu/VisualSettings.js:182
#: settings/Menu/VisualSettings.js:183
msgid "Wide"
msgstr ""

#: settings/Menu/VisualSettings.js:184 settings/MenuButtonPage.js:439
msgid "Custom"
msgstr ""

#: settings/Menu/VisualSettings.js:186
msgid "Grid Menu Items"
msgstr ""

#: settings/Menu/VisualSettings.js:187
msgid "Apps, Pinned Apps, Shortcuts, Grid Search Results"
msgstr ""

#: settings/Menu/VisualSettings.js:217
msgid "Applications"
msgstr ""

#: settings/Menu/VisualSettings.js:218
msgid "Apps, Pinned Apps, Items within Category, List Search Results"
msgstr ""

#: settings/Menu/VisualSettings.js:225
msgid "Directory / Application / Other Shortcuts, Power Menu"
msgstr ""

#: settings/Menu/VisualSettings.js:231
msgid "Application Categories"
msgstr ""

#: settings/Menu/VisualSettings.js:237
msgid "Button Widgets"
msgstr ""

#: settings/Menu/VisualSettings.js:238
msgid "Power Buttons, Unity Bottom Bar, Mint Side Bar, etc"
msgstr ""

#: settings/Menu/VisualSettings.js:244
msgid "Miscellaneous"
msgstr ""

#: settings/Menu/VisualSettings.js:245
msgid "Avatar, Search, Navigation Icons"
msgstr ""

#: settings/Menu/VisualSettings.js:272
msgid "Extra Small"
msgstr ""

#: settings/Menu/VisualSettings.js:276
msgid "Extra Large"
msgstr ""

#: settings/Menu/VisualSettings.js:279 settings/MenuButtonPage.js:61
msgid "Hidden"
msgstr ""

#: settings/Menu/VisualSettings.js:298
msgid "Custom Grid Icon Size"
msgstr ""

#: settings/Menu/VisualSettings.js:346 settings/MenuButtonPage.js:190
msgid "Icon Size"
msgstr ""

#: settings/MenuButtonPage.js:16
msgid "Menu Button"
msgstr ""

#: settings/MenuButtonPage.js:52
msgid "Menu Button Appearance"
msgstr ""

#: settings/MenuButtonPage.js:58 settings/MenuButtonPage.js:141
msgid "Text"
msgstr ""

#: settings/MenuButtonPage.js:59
msgid "Icon and Text"
msgstr ""

#: settings/MenuButtonPage.js:60
msgid "Text and Icon"
msgstr ""

#: settings/MenuButtonPage.js:63
msgid "Display Style"
msgstr ""

#: settings/MenuButtonPage.js:100
msgid "Padding"
msgstr ""

#: settings/MenuButtonPage.js:153
msgid "Menu Button Icon"
msgstr ""

#: settings/MenuButtonPage.js:167
msgid "Choose a new icon"
msgstr ""

#: settings/MenuButtonPage.js:201
msgid "Menu Button Styling"
msgstr ""

#: settings/MenuButtonPage.js:229
msgid "Background colors required if set to 0"
msgstr ""

#: settings/MenuButtonPage.js:377
msgid "ArcMenu Icons"
msgstr ""

#: settings/MenuButtonPage.js:381
msgid "Icons"
msgstr ""

#: settings/MenuButtonPage.js:401
msgid "Distros"
msgstr ""

#: settings/MenuButtonPage.js:407
msgid "Legal Disclaimer"
msgstr ""

#: settings/MenuButtonPage.js:504
msgid "Custom Icon"
msgstr ""

#: settings/MenuButtonPage.js:549
msgid "Legal Disclaimer for Distro Icons"
msgstr ""

#: settings/MenuPage.js:24
msgid "Menu"
msgstr ""

#: settings/MenuPage.js:32
msgid "How should the menu look?"
msgstr ""

#: settings/MenuPage.js:39
msgid "Menu Layout"
msgstr ""

#: settings/MenuPage.js:40
msgid "Choose a layout style for the menu"
msgstr ""

#: settings/MenuPage.js:55
msgid "Menu Theme"
msgstr ""

#: settings/MenuPage.js:56
msgid "Modify menu colors, font size, and border"
msgstr ""

#: settings/MenuPage.js:65
msgid "Menu Visual Appearance"
msgstr ""

#: settings/MenuPage.js:66
msgid "Change menu height, width, location, and icon sizes"
msgstr ""

#: settings/MenuPage.js:75
msgid "Fine Tune"
msgstr ""

#: settings/MenuPage.js:76
msgid "Adjust less commonly used visual settings"
msgstr ""

#: settings/MenuPage.js:85
msgid "What should show on the menu?"
msgstr ""

#: settings/MenuPage.js:92
msgid "Settings specific to the current menu layout"
msgstr ""

#: settings/MenuPage.js:112
msgid "Directory Shortcuts"
msgstr ""

#: settings/MenuPage.js:145
msgid "Choose which power options to show and the display style"
msgstr ""

#: settings/MenuPage.js:156
msgid "Extra Categories"
msgstr ""

#: settings/MenuPage.js:158
msgid "Add or remove additional custom categories"
msgstr ""

#: settings/MenuPage.js:168
msgid "What should show on the context menu?"
msgstr ""

#: settings/MenuPage.js:173
msgid "Modify ArcMenu Context Menu"
msgstr ""
